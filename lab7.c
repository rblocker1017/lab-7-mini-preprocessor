#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void remc(char *s); // removes comments
void inchand(char *s, FILE *fp); // handles #includes

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Please supply an input and an output file\n");
        exit(2);
    }
    
    FILE *fpin;
    fpin = fopen(argv[1], "r");
    if (!fpin)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    FILE *fpout;
    fpout = fopen(argv[2], "w");
    if (!fpout)
    {
        printf("Can't open %s for writing\n", argv[2]);
        exit(1);
    }
    
    char line[100];
    while(fgets(line, 100, fpin) != NULL)
    {
        remc(line);
        inchand(line, fpout);
        fprintf(fpout, "%s", line);
    }
    
    fclose(fpin);
    fclose(fpout);
}

void remc(char *s)
{
    for(int i = 0; i < strlen(s); i++)
    {
        if(s[i] == '/' && s[i+1] == '/')
        {
            s[i] = '\n';
            s[i+1] = '\0';
        }
    }
}

void inchand(char *s, FILE *fp)
{
    char *include = "#include ";
    int startline = 1;
    
    for(int i = 0; i < strlen(include); i++)
    {
        if(include[i] != s[i])
        {
            startline = 0;
        }
    }
    
    if(startline == 1)
    {
        char *fname = &s[9];
        fname[strlen(fname)-1] = '\0';
        
        s[0] = '\n';
        s[1] = '\0';
        
        FILE *fptemp;
        fptemp = fopen(fname, "r");
        if (!fptemp)
        {
            printf("Can't open %s for reading\n", fname);
            exit(1);
        }
        
        char line[100];
        while(fgets(line, 100, fptemp) != NULL)
        {
            fprintf(fp, "%s", line);
        }
        fclose(fptemp);
    }
}